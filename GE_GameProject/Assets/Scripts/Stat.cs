﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Stat
{
    // Referencing the bar script
    private BarScript bar;
    
    // Values for health and armor
    protected float maxValue;
    protected float currValue;

    // Get and set the currValue for the bar
    public virtual float CurrValue
    {

        get
        {
            return currValue;
        }

        set
        {
            this.currValue = Mathf.Clamp(value, 0, MaxValue);
            bar.Value = currValue;
        }
    }

    // Get and set the maxValue for the bar
    public virtual float MaxValue
    {
        get
        {
           return maxValue;
        }

        set
        {
            this.maxValue = value;
            bar.MaxValue = maxValue;
        }
    }

    // Initialize both the max and currValue for the bar
    public virtual void Initialize()
    {
        this.MaxValue = maxValue;
        this.CurrValue = currValue;
    }
}
