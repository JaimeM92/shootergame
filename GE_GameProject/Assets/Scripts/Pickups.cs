﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickups : MonoBehaviour
{
    void Update()
    {
        // Rotate pickup to make stand out in game world
        transform.Rotate(new Vector3(0, 4f, 0));
    }

    // Destroy object when colliding with player
    public virtual void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }


}
