﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipWeapon : MonoBehaviour
{
    public Weapon equippedWeapon;
    public Transform attachPoint;

    //[SerializeField] private Weapon weaponLeftHand;
   // [SerializeField] private Weapon weaponRightHand;

    //[SerializeField] private Transform playerLeftHand;
    //[SerializeField] private Transform playerRightHand;
    //[SerializeField] private Transform rightHandParent;

    //[SerializeField] private Vector3 playerRightHandPos;
    //[SerializeField] private Vector3 rightHandParentPos;
    //[SerializeField] private Quaternion playerRightHandRot;

    private Animator anim;

    static readonly Vector3 offset = new Vector3(.02f, .04f, 0);

    void Awake()
    {
        anim = GetComponent<Animator>();

        //rightHandParent = playerRightHand.parent;
    }

    // Start is called before the first frame update
    void Start()
    {
        EquippedWeapon(equippedWeapon);
    }
    /*
    // Update is called once per frame
    void LateUpdate()
    {
        //playerRightHandPos = weaponRightHand.rightHand.position;
       // playerRightHandRot = weaponRightHand.rightHand.rotation;
        //rightHandParentPos = rightHandParent.position;
    }

    private void OnAnimatorIK()
    {
        Vector3 targetPos = weaponRightHand.rightHand.position + weaponRightHand.rightHand.rotation * offset;
        targetPos += playerRightHand.position + playerRightHand.rotation * offset - rightHandParent.position;

        anim.SetIKPosition(AvatarIKGoal.RightHand, Vector3.zero);
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
    }*/

    public void EquippedWeapon(Weapon weapon)
    {
        equippedWeapon = Instantiate(weapon) as Weapon;

        equippedWeapon.transform.SetParent(attachPoint);
          equippedWeapon.transform.localPosition = attachPoint.transform.localPosition;
          equippedWeapon.transform.localRotation = attachPoint.transform.localRotation;
        //equippedWeapon.transform.localPosition = weapon.transform.localPosition;
        //equippedWeapon.transform.localRotation = weapon.transform.localRotation;
    }
}
