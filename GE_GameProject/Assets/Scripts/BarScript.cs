﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScript : MonoBehaviour
{
    // Deteremins how fast and smooth the bars change
    protected float fillAmount;
    protected float lerpSpeed;

    // Calls upon bar image
    protected Image content;

    public virtual float MaxValue { get; set; }

    // Fill amount is equal to the Map
    public virtual float Value
    {
        set
        {
            fillAmount = Map(value, 0, MaxValue, 0, 1);
        }
    }

    // Update is called once per frame
    public virtual void Update()
    {
        HandleBar();
    }

    // Adjust Bar
    public virtual void HandleBar()
    {
        if (fillAmount != content.fillAmount)
        {
            content.fillAmount = Mathf.Lerp(content.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
        }

    }

    // Calculate the amount that is in bar
    public virtual float Map(float value, float inMin, float inMax, float outMin, float outMax)
    {

        return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }
}
