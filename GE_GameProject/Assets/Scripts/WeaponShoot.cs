﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponShoot : MonoBehaviour
{
    [Header("Bullet Data")]
    [Tooltip("Variables for the bullet")]
    public M4Bullet bullet;
    [Space(10)]
    public float bulletSpeed;
    public float rateOfFire;
    private float shotCounter;
    public bool isFiring;

    [SerializeField] protected Transform muzzle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame - Firing Mechanism
    void Update()
    {
        if (isFiring)
        {
            shotCounter -= Time.deltaTime;
            if (shotCounter <= 0)
            {
                shotCounter = rateOfFire;
                M4Bullet newbullet = Instantiate(bullet, muzzle.position, muzzle.rotation) as M4Bullet;
                newbullet.speed = bulletSpeed;
            }
        }

        else
        {
            shotCounter = 0;
        }
    }
}
