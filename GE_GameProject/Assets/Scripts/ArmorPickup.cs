﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorPickup : Pickups
{
    [Header("Armor Values")]
    [Tooltip("Value that will be added on to the player's armor amount")]
    public float ArmorValue;

    // Use Pawn component of Player
    [SerializeField] private Pawn pawn;

    public override void OnTriggerEnter(Collider other)
    {
       if (pawn.armor.currValue < pawn.armor.maxValue)
       {
           Destroy(this.gameObject);
           pawn.armor.currValue = pawn.armor.currValue + ArmorValue;
       }
        
    }
}
