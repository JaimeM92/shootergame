﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Target that camera will follow
    [SerializeField] private Transform target;

    // Distance camera will be from target
    [SerializeField] private Vector3 offset;

    private Transform tf;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        tf.position = target.position + offset;
        tf.LookAt(target.position);
    }
}
