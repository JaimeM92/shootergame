﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : Controller
{
    // References weapon for shooting
    [SerializeField] private WeaponShoot shoot;

    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        Move();
        Rotate();
        
        // Press to shoot
        if(Input.GetKey(KeyCode.Space))
        {
            shoot.isFiring = true;
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            shoot.isFiring = false;
        }

        // Toggle to Sprint animator
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Sprint();
        }

        // Toggle to Crouch animator
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Crouch();
        }
    }
}
