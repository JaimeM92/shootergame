﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ArmorStat : Stat
{
    [Header("Armor Values")]
    public ArmorBar armor;
    [Space(10)]
    [Header("Values")]
    [Tooltip("Variables to set values for Health and Armor")]
    public new float maxValue;
    public new  float currValue;

    // Get and set the currValue for the bar
    public override float CurrValue
    {
        get
        {
            return currValue;
        }

        set
        {
            this.currValue = Mathf.Clamp(value, 0, MaxValue);
            armor.Value = currValue;
        }
    }

    // Get and set the maxValue for the bar
    public override float MaxValue
    {
        get
        {
            return maxValue;
        }

        set
        {
            this.maxValue = value;
            armor.MaxValue = maxValue;
        }
    }

    // Initialize both the max and currValue for the bar
    public override void Initialize()
    {
        this.MaxValue = maxValue;
        this.CurrValue = currValue;
    }
}
