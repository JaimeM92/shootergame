﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class HealthStat : Stat
{
    [Header("Health values")]
    public HealthBar health;
    [Space(10)]
    [Header("Values")]
    [Tooltip("Variables to set values for Health and Armor")]
    public new float maxValue;
    public new float currValue;

    // Get and set the currValue for the bar
    public override float CurrValue
    {
        get
        {
            return currValue;
        }

        set
        {
            this.currValue = Mathf.Clamp(value, 0, MaxValue);
            health.Value = currValue;
        }
    }

    // Get and set the maxValue for the bar
    public override float MaxValue
    {
        get
        {
            return maxValue;
        }

        set
        {
            this.maxValue = value;
            health.MaxValue = maxValue;
        }
    }

    // Initialize both the max and currValue for the bar
    public override void Initialize()
    {
        this.MaxValue = maxValue;
        this.CurrValue = currValue;
    }
}
