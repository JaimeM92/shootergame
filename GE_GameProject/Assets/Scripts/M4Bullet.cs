﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class M4Bullet : Bullet
{
    [SerializeField] private new float bulletLifeCounter;
    public new float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public override void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
}
