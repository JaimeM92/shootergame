﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Pawn : MonoBehaviour
{
    [Header("Health / Armor")]
    [Tooltip("Values to determine how much health character has")]
    [SerializeField]
    public HealthStat health;
    public ArmorStat armor;

    void Awake()
    {
        health.Initialize();
        armor.Initialize();
    }
}
