﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmorBar : BarScript
{
    [Header("Fill amount")]
    [Tooltip("Deteremins how fast and smooth the bars change")]
    public new float fillAmount;
    public new float lerpSpeed;

    // Calls upon new bar image
    public new Image content;

    public override float MaxValue { get; set; }

    // Fill amount is equal to the Map
    public override float Value
    {
        set
        {
            fillAmount = Map(value, 0, MaxValue, 0, 1);
        }
    }

    // Update is called once per frame
    public override void Update()
    {
        HandleBar();
    }

    // Adjust Bar
    public override void HandleBar()
    {
        if (fillAmount != content.fillAmount)
        {
            content.fillAmount = Mathf.Lerp(content.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
        }

    }

    // Calculate the amount that is in bar
    public override float Map(float value, float inMin, float inMax, float outMin, float outMax)
    {

        return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }
}

