﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [Header("Movement Values")]
    [Tooltip("Values to determine the speed in seconds and degrees")]
    [SerializeField] protected float charSpeed;
    [SerializeField] protected float RotateSpeed;


    // Access Object Components
    [Header("Components")]
    [Tooltip("Animation to call to and the Transform of object")]
    [SerializeField] private Animator anim;
    [SerializeField] private Transform tf; 

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move()
    {
        // Move player through animator 
        // Move along Horizontal on X-Axis and Vertical on Z-Axis
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        moveDirection = Vector3.ClampMagnitude(moveDirection, 1f);
        moveDirection *= charSpeed;
        // Set the float value within the animator while moving
        anim.SetFloat("Horizontal", moveDirection.x);
        anim.SetFloat("Vertical", moveDirection.z);

    }

    public void Rotate()
    {
        // Set value to rotate player
        float horiz = Input.GetAxis("Mouse X");

        // Rotate player
        tf.Rotate(new Vector3(0, horiz, 0) * RotateSpeed);
    }

    //
    public void Sprint()
    {
            anim.SetTrigger("Sprint");
    }

    //
    public void Crouch()
    {
            anim.SetTrigger("Crouch");
    }

    //
    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

}
