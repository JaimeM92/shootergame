﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Variable to determine life of shell 
    protected float bulletLifeCounter;
    protected float speed;

    // Update is called once per frame
    public virtual void Update()
    {
        // Shell life counter when fired
        if (bulletLifeCounter > 0)
        {
            bulletLifeCounter -= Time.deltaTime;
        }

        // Destroy shell when it reaches 0
        else
        {
            enabled = false;

            Destroy(gameObject);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Target")
        {
        
            Destroy(other.gameObject);
        }
    }
}
