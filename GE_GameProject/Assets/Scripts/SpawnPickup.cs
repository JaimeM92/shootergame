﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPickup : MonoBehaviour
{
    [Header("Spawn Info")]
    [Tooltip("Data for spawning pickups")]
    public GameObject pickup;
    [Space(5)]
    public bool stopSpawn = false;
    public float spawnTime;
    public float spawnDelay;

    void Start()
    {
        // At start spawn object
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
    }

    // Spawn Assigned object
    public void SpawnObject()
    {
        Instantiate(pickup, transform.position, transform.rotation);
        
        // needed for when to stop spawning (maybe for future use)
        /*
        if(stopSpawn)
        {
            CancelInvoke("SpawnObject");
        }
        */
    }
}
