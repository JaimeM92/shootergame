﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform leftHand;
    public Transform rightHand;

   

 
  
    // Used for switching between weapon animations
    public enum WeaponAnimType
    {
        Rifle = 0,
        Handgun = 1
    }

    [SerializeField] protected WeaponAnimType animType;

    // Start is called before the first frame update
    public virtual void Start() { }

    public virtual void Update() { }

    // Weapon shoots
    public virtual void Shoot() { }
}
